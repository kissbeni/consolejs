var vConsoleGlobalStylesheet = (function() {
    var style = document.createElement("style");
    style.appendChild(document.createTextNode("")); // WebKit hack :(
    document.head.appendChild(style);
    return style.sheet;
})();

// I was too lazy to create separate css files...
vConsoleGlobalStylesheet.insertRule('virtual-console { font-family: "Consolas" }');
vConsoleGlobalStylesheet.insertRule('virtual-console span { word-wrap: break-word; }');
vConsoleGlobalStylesheet.insertRule('virtual-console .cin { color: #753658 }');

vConsoleGlobalStylesheet.insertRule('virtual-console .cursor {' +
    'width: 9px;' +
    'height: 15px;' +
    'border: 0px;' +
    'color: transparent;' +
    'text-align: center;' +
'}');

vConsoleGlobalStylesheet.insertRule('virtual-console .cursor-container {' +
    'position: relative;' +
    'top: -1px;' +
    'left: 0px;' +
    'display: inline-block;' +
'}');

vConsoleGlobalStylesheet.insertRule('virtual-console a {' +
    'color: inherit;' +
    'text-decoration: none;' +
'}');

vConsoleGlobalStylesheet.insertRule('virtual-console a:hover {' +
    'text-decoration: underline;' +
'}');

var colorIDs = [];
colorIDs[0] = "RESET";
colorIDs[39] = "RESET";

colorIDs[30] = "#000";
colorIDs[31] = "#F00";
colorIDs[32] = "green";
colorIDs[33] = "#FF0";
colorIDs[34] = "#00F";
colorIDs[35] = "#F0F";
colorIDs[36] = "#0FF";
colorIDs[37] = "#D3D3D3";

colorIDs[90] = "#808080";
colorIDs[91] = "#F55";
colorIDs[92] = "#0F0";
colorIDs[93] = "#FF5";
colorIDs[94] = "#55F";
colorIDs[95] = "#F5F";
colorIDs[96] = "#5FF";
colorIDs[97] = "#FFF";

var vConsoleObject = {
    prototype: Object.create(HTMLElement.prototype, {
        createdCallback: {
            value: function() {
                this.coutSpan = document.createElement("SPAN");
                this.appendChild(this.coutSpan);

                this.cinSpan = document.createElement("SPAN");
                this.cinSpan.className = "cin";
                this.appendChild(this.cinSpan);

                this.cursorContainer = document.createElement("DIV");
                this.cursorContainer.className = "cursor-container"
                this.cursor = document.createElement("INPUT");
                this.cursor.setAttribute("type", "text");
                this.cursor.className = "cursor";
                this.cursorContainer.appendChild(this.cursor);
                this.appendChild(this.cursorContainer);

                this.defaultForegroundColor = "black";

                // I/O data
                this.cinText = "";
                this.coutData = [];
                this.enableInput = true;
                this.keepInput = true;
                this.blinkState = 0;

                this.inputHistory = [];
                this.currentInputHistoryIndex = 0;

                // Default handler
                this.consoleHandler = function(sender,args) {}
                this.handleKey = function(sender,key_event) {return true;}
                this.notifyPaste = function(sender) {}

                this.cursor.onkeydown = function(e) {
                    var t = null;

                    if (typeof e.path === "undefined") {
                        t = e.target;

                        while (t.tagName != "VIRTUAL-CONSOLE") {
                            t = t.parentNode;
                        }
                    }
                    else
                        t = e.path[2];

                    if (!t.handleKey(t,e)) {
                        t.cursor.value = "";
                        t.blinkState = 2;
                        return;
                    }
                    
                    if (t.enableInput) {
                        if (e.key == "v" && e.ctrlKey) {
                            setTimeout(function(t) {
                                t.cinText += t.cursor.value;
                                t.cursor.value = "";
                                t.blinkState = 2;
                                t.updateConsole();
                                t.notifyPaste(t);
                            }, 1, t);

                            return;
                        }

                        if (e.key.length == 1) {
                            if (t.currentInputHistoryIndex < 0)
                                t.cinText = t.inputHistory[t.getCurrentHistoryIndex()];
                            
                            t.currentInputHistoryIndex = 0;

                            t.cinText += e.key;
                        } else {
                            switch (e.key) {
                                case "Enter":
                                    t.finishInput(true);
                                    break;
                                case "Backspace":
                                    if (t.currentInputHistoryIndex < 0)
                                        t.cinText = t.inputHistory[t.getCurrentHistoryIndex()];
        
                                    t.currentInputHistoryIndex = 0;
                                    t.cinText = t.cinText.substring(0, t.cinText.length - 1);
                                    break;
                                case "ArrowUp":
                                    if (t.currentInputHistoryIndex > -t.inputHistory.length)
                                        t.currentInputHistoryIndex--;
                                    break;
                                case "ArrowDown":
                                    if (t.currentInputHistoryIndex < 0)
                                        t.currentInputHistoryIndex++;
                                    break;
                                case "Control":
                                    break;
                                default:
                                    console.log(e);
                                    break;
                            }
                        }
                    }

                    t.blinkState = 2;
                    t.cursor.value = "";
                    t.updateConsole();
                }

                setInterval(function(x) {
                    if (x.enableInput)
                        x.cursor.focus();

                    if (x.blinkState <= 0) {
                        x.blinkState = 1;
                        x.cursor.style.background = "none";
                    } else {
                        x.blinkState--;
                        x.cursor.style.background = x.defaultForegroundColor;
                    }
                }, 500, this);

                this.updateConsole();
            }
        },
        write: {
            value: function(what, color) {
                this.writeTextInternal(what, color, false);
                this.updateConsole();
            }
        },
        writeLine: {
            value: function(what, color) {
                if (what === undefined)
                    what = "";

                this.writeTextInternal(what, color, true);
                this.updateConsole();
            }
        },
        writeTextInternal: {
            value: function(str, c, isWriteLine /* we need to convert the type first, then add \n */) {
                if (str == null || str == undefined)
                    return;

                if (c === undefined)
                    c = null;

                if (typeof str === "object")
                    str = str + ": " + JSON.stringify(str);
                else if (typeof str !== "string")
                    str = str.toString();

                if (isWriteLine)
                    str += "\n";

                var unixColorMatches = str.match("\x1B\\[(\\d+)m");
                if (unixColorMatches) {
                    var lastColor = c;
                    do {
                        this.writeTextInternal(str.substring(0, unixColorMatches.index), lastColor, false);

                        var colorID = parseInt(unixColorMatches[1]);
                        if (colorIDs[colorID] !== undefined) {
                            lastColor = colorIDs[colorID];

                            if (lastColor == "RESET")
                                lastColor = c;
                        }

                        str = str.substring(unixColorMatches.index + unixColorMatches[0].length);
                    } while (unixColorMatches = str.match("\x1B\\[(\\d+)m"));

                    if (str.length > 0)
                        this.writeTextInternal(str, lastColor, false);

                    return;
                }

                var matches = str.match(/\bhttps?:\/\/[^"'\(\)\]\[<>\s]+\/[^"'\(\)\]\[<>:\s]+/gi);
                
                var iswl = str.endsWith("\n");
                var splitted = str.split('\n');

                for (var i = 0; i < splitted.length - (iswl ? 1 : 0); i++) {
                    var foundNoEndl = false;
                    var text = splitted[i];
                    var newl = (i != splitted.length - 1) || iswl;

                    for (var j = 0; j < this.coutData.length; j++) {
                        if (!this.coutData[j].hasEndl) {
                            if (matches)
                                for (var k = 0; k < matches.length; k++) {
                                    var pos;
                                    while ((pos = text.indexOf(matches[k])) != -1) {
                                        if (pos > 0)
                                            this.coutData[j].value.push({ text: text.substring(0, pos), color: c, isUrl: false });
                                        
                                        this.coutData[j].value.push({ text: text.substring(pos, pos + matches[k].length), color: c, isUrl: true });
                                        text = text.substring(pos + matches[k].length);
                                    }
                                }
                            
                            if (text.length > 0)
                                this.coutData[j].value.push({ text: text, color: c, isUrl: false });

                            this.coutData[j].hasEndl = newl;
                            this.coutData[j].needRewrite = true;
                            foundNoEndl = true;
                            break;
                        }
                    }

                    if (!foundNoEndl) {
                        var v = [];

                        if (matches) {
                            for (var j = 0; j < matches.length; j++) {
                                var pos;
                                while ((pos = text.indexOf(matches[j])) != -1) {
                                    if (pos > 0)
                                        v.push({ text: text.substring(0, pos), color: c, isUrl: false });

                                    v.push({ text: text.substring(pos, pos + matches[j].length), color: c, isUrl: true });
                                    text = text.substring(pos + matches[j].length);
                                }
                            }

                            if (text.length > 0)
                                v.push({ text: text, color: c, isUrl: false });
                        }
                        else
                            v.push({ text: text, color: c, isUrl: false });

                        this.coutData.push({
                            value: v,
                            hasEndl: newl,
                            needRewrite: true
                        });
                    }
                }
            }
        },
        finishInput: {
            value: function(invokedInternally) {
                if (this.currentInputHistoryIndex < 0)
                    this.cinText = this.inputHistory[this.getCurrentHistoryIndex()];

                if (this.inputHistory.length == 0 || this.inputHistory[this.inputHistory.length - 1] != this.cinText)
                    this.inputHistory.push(this.cinText);

                this.currentInputHistoryIndex = 0;
                var args = [], splitted = [];//, splitted = this.cinText.split(' ');
                
                var buffer = "";
                var inSingleQuote = false, inDoubleQuote = false;

                for (var i = 0; i < this.cinText.length; i++) {
                    var chr = this.cinText[i];

                    if (!(inSingleQuote || inDoubleQuote) && (chr == ' ' || chr == '\t')) {
                        splitted.push(buffer);
                        buffer = "";
                        continue;
                    }

                    if (chr == '"' && !inSingleQuote) {
                        if (inDoubleQuote) {
                            inDoubleQuote = false;
                        } else {
                            inDoubleQuote = true;
                        }

                        continue;
                    }

                    if (chr == '\"' && !inDoubleQuote) {
                        if (inSingleQuote) {
                            inSingleQuote = false;
                        } else {
                            inSingleQuote = true;
                        }

                        continue;
                    }

                    buffer += chr;
                }

                splitted.push(buffer);

                for (var i = 0; i < splitted.length; i++) {
                    if (splitted[i] == "") continue;

                    var numberResult = NaN;
                    if (isNaN(numberResult = Number(splitted[i])))
                        args.push({ value: splitted[i], str: splitted[i], typeName: 'string', isNumber: false })
                    else {
                        if (parseInt(splitted[i]) == numberResult) // is integer
                            args.push({ value: numberResult, str: splitted[i], typeName: 'integer', isNumber: true })
                        else
                            args.push({ value: numberResult, str: splitted[i], typeName: 'double', isNumber: true })
                    }
                }

                if (this.keepInput)
                    this.writeLine(this.cinText);

                if (inSingleQuote) {
                    this.writeLine("warning: missing single quote\n", "orange");
                } else if (inDoubleQuote) {
                    this.writeLine("warning: missing double quote\n", "orange");
                }

                try {
                    this.consoleHandler(this, { raw: this.cinText, parsed: args, count: args.length });
                } catch (e) {
                    this.writeLine(e.stack, "red");
                }

                this.cinText = "";
                !invokedInternally && this.updateConsole();
            }
        },
        updateConsole: {
            value: function() {
                // console.log(this.coutData);
                var spans = this.coutSpan.getElementsByTagName("SPAN");
                var flag = false;
                for (var i = 0; i < this.coutData.length; i++) {
                    flag = false;
                    var data = this.coutData[i];
                    if (!data.needRewrite)
                        continue;

                    data.needRewrite = false;

                    for (var x = 0; x < spans.length; x++) {
                        if (spans[x].getAttribute("data-line-id") == i) {
                            spans[x].innerHTML = "";
                            
                            for (var k = 0; k < data.value.length; k++) {
                                var value = data.value[k];
                                var prefix = "", suffix = "";

                                if (value.isUrl)
                                    prefix += "<a href='" + value.text.replace(/"/g, "").replace(/'/g, "").replace(/>/g, "") + "' target='_blank'>", suffix = "</a>";

                                if (value.color != null)
                                    prefix += "<font color='" + value.color + "'>", suffix += "</font>";

                                spans[x].innerHTML += prefix + value.text
                                    .replace(/&/g, '&amp;')
                                    .replace(/</g, '&lt;')
                                    .replace(/>/g, '&gt;')
                                    .replace(/"/g, '&quot;')
                                    .replace(/ /g, '&nbsp;') + suffix + ((k == data.value.length - 1 && data.hasEndl) ? "<br/>" : "");
                            }
                            
                            flag = true;
                            break;
                        }
                    }

                    if (!flag) {
                        var e = document.createElement("SPAN");
                        e.className = "static-line";
                        e.setAttribute("data-line-id", i);

                        for (var k = 0; k < data.value.length; k++) {
                            var value = data.value[k];
                            var prefix = "", suffix = "";

                            if (value.isUrl)
                                prefix += "<a href='" + value.text.replace(/"/g, "").replace(/'/g, "").replace(/>/g, "") + "' target='_blank'>", suffix = "</a>";

                            if (value.color != null)
                                prefix += "<font color='" + value.color + "'>", suffix += "</font>";

                            e.innerHTML += prefix + value.text
                                    .replace(/&/g, '&amp;')
                                    .replace(/</g, '&lt;')
                                    .replace(/>/g, '&gt;')
                                    .replace(/"/g, '&quot;')
                                    .replace(/ /g, '&nbsp;') + suffix + ((k == data.value.length - 1 && data.hasEndl) ? "<br/>" : "");
                        }

                        this.coutSpan.appendChild(e);
                        spans = this.coutSpan.getElementsByTagName("SPAN");
                    }
                }

                for (var x = 0; x < spans.length; x++) {
                    spans[x].style.color = this.defaultForegroundColor;
                }

                if (this.currentInputHistoryIndex >= 0) {
                    this.cinSpan.innerHTML = this.cinText
                        .replace(/&/g, '&amp;')
                        .replace(/</g, '&lt;')
                        .replace(/>/g, '&gt;')
                        .replace(/"/g, '&quot;')
                        .replace(/\n/g, '<br/>')
                        .replace(/ /g, '&nbsp;');
                } else {
                    this.cinSpan.innerHTML = this.inputHistory[this.getCurrentHistoryIndex()]
                        .replace(/&/g, '&amp;')
                        .replace(/</g, '&lt;')
                        .replace(/>/g, '&gt;')
                        .replace(/"/g, '&quot;')
                        .replace(/\n/g, '<br/>')
                        .replace(/ /g, '&nbsp;');
                }

                this.cursor.blinkState = 2;
                this.cursor.style.background = "black";

                this.scrollTop = this.scrollHeight - this.clientHeight;
            }
        },
        getCurrentHistoryIndex: {
            value: function() {
                if (this.currentInputHistoryIndex >= 0)
                    return -1;

                var arrayIndex = this.inputHistory.length + this.currentInputHistoryIndex; // history index is always be negative, so this is fine here

                if (arrayIndex > this.inputHistory.length - 1)
                    arrayIndex = this.inputHistory.length - 1;

                return arrayIndex;
            }
        }
    }
)};

if (typeof document.registerElement === "function") {
    document.registerElement('virtual-console', vConsoleObject);
}

function vConsoleFirefoxFix() { // HACK: Firefox pls...
    if (typeof document.registerElement !== "function") {
        vConsoleGlobalStylesheet.insertRule('virtual-console .cursor-container { top: 0px; }', 4);
        vConsoleGlobalStylesheet.insertRule('virtual-console .cursor { width: 7px; }', 4);

        var elements = document.getElementsByTagName("virtual-console");
        for (var i = 0; i < elements.length; i++) {
            elements[i].createdCallback = vConsoleObject.prototype.createdCallback;
            elements[i].write = vConsoleObject.prototype.write;
            elements[i].writeLine = vConsoleObject.prototype.writeLine;
            elements[i].writeTextInternal = vConsoleObject.prototype.writeTextInternal;
            elements[i].updateConsole = vConsoleObject.prototype.updateConsole;
            elements[i].getCurrentHistoryIndex = vConsoleObject.prototype.getCurrentHistoryIndex;
            elements[i].createdCallback();
        }
    }
}
